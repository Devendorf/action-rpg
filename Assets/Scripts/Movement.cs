﻿using UnityEngine;

[RequireComponent(typeof(CharacterAnimator))]

public class Movement : MonoBehaviour
{
    [SerializeField] private float _rechargeTime;

    protected float Speed
    {
        get
        {
            return isStoped
                ? 0
                : speed;
        }
        set
        {
            speed = value;
        }
    }

    public float DistanceRemain
    {
        get
        {
            return (transform.position - MoveTarget).magnitude;
        }
    }

    [SerializeField] public float speed;
    public Vector3 MoveTarget;
    protected CharacterAnimator _characterAnimator;
    public bool isStoped = true;

    protected virtual void Start()
    {
        _characterAnimator = GetComponent<CharacterAnimator>();
        //AttackButton.onAttack += StopMovement;
        //AttackButton.onStoped += StartMovement;
        _characterAnimator.onAttackStateChange += SetPause;
    }

    private void SetPause(bool value)
    {
        isStoped = value;
    }
    private void OnDestroy()
    {
        _characterAnimator.onAttackStateChange -= SetPause;
        //AttackButton.onAttack -= StopMovement;
        //AttackButton.onStoped -= StartMovement;
    }
    protected void StartMovement(Vector3 target)
    {
        isStoped = false;
        _characterAnimator.Move(target);
    }

    public void StopMovement()
    {
        isStoped = true;
        _characterAnimator.Stop();
    }

    public void Move(Vector3 target)
    {
        MoveTarget = target;
        if (DistanceRemain > 2)
        {
            StartMovement(target);
        }
    }

    private void FixedUpdate()
    {
        MoveCharacter();
    }
    protected virtual void MoveCharacter()
    {
        if (!isStoped)
        {
            transform.position = Vector2.MoveTowards(transform.position, MoveTarget, Speed * Time.fixedDeltaTime);
            //print(_distanceRemain);
            if (DistanceRemain <= 2)
                StopMovement();
        }
    }
}
