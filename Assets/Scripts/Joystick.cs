﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class Joystick : MonoBehaviour
{
    [SerializeField] private Text _text;
    [SerializeField] private GameObject _touchMarker;
    [SerializeField] private PlayerMovement _playerMovement;
    private RectTransform _rectTransform;
    private float _radius;
    private float _zoom;
    private bool _isTouched;
    private Vector3 _touchPosition;
    private Vector3 _targetVector;
    private int _touchId;
    private Touch _touch;

    private void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
        _touchId = -1;
    }

    private void Update()
    {
        //if (Input.GetMouseButtonDown(0) && _touch.fingerId==0)
        if (Input.touchCount > 0)
        {
            if (_touchId >= 0)
            {
                _touch = Input.GetTouch(_touchId);

                if (_touch.phase == TouchPhase.Moved || _touch.phase == TouchPhase.Stationary)
                {
                    _touchPosition = _touch.position;
                    //text.text = _touchPosition.ToString();
                    _targetVector = _touchPosition - _rectTransform.position;
                    CalculateMarkerPosition();
                }
                else
                    _touchId = -1;
            }
            else
            {
                int id = 0;
                while (id < Input.touchCount)
                {
                    _touch = Input.GetTouch(id);
                    if (_touch.phase == TouchPhase.Began)
                    {
                        _zoom = _rectTransform.lossyScale.x;
                        //_touchPosition = Input.mousePosition;
                        _touchPosition = _touch.position;
                        _targetVector = _touchPosition - _rectTransform.position;
                        //text.text = Input.mousePosition.ToString() + "-" + tmpTouch.position.ToString();
                        _radius = _rectTransform.rect.width / 2;
                        _isTouched = isTouchInCircle();
                        if (_isTouched)
                        {
                            _touchId = id;
                            break;
                        }
                    }
                    id++;
                }
            }
        }
        else
        {
            ReturnMarker();
        }
    }

    private bool isTouchInCircle()
    {
        return (_radius > (_targetVector.magnitude / _zoom));
    }

    private void CalculateMarkerPosition()
    {
        float R = isTouchInCircle()
            ? _targetVector.magnitude
            : _radius * _zoom;
        Vector3 resultVector = R * _targetVector / _targetVector.magnitude / _zoom;
        SetMarkerPosition(resultVector, _touchMarker.transform.localPosition);
    }

    private void ReturnMarker()
    {
        SetMarkerPosition(Vector3.zero, Vector3.zero);
    }

    private void SetMarkerPosition(Vector3 markerPosition, Vector3 moveTarget)
    {
        _touchMarker.transform.localPosition = markerPosition;
        _playerMovement.Move(moveTarget);
        //_controller.MoveTarget = moveTarger;
    }
}
