﻿using UnityEngine;

public class PlayerMovement : Movement
{
    private Vector3 _cameraOffset;

    protected override void Start()
    {
        base.Start();
        _cameraOffset = Camera.main.transform.position - transform.position;
        AttackButton.onAttack += StopMovement;
        AttackButton.onStoped += StartMovement;
    }
    private void OnDestroy()
    {
        AttackButton.onAttack -= StopMovement;
        AttackButton.onStoped -= StartMovement;
    }
    private void StartMovement()
    {
        isStoped = false;
    }

    protected override void MoveCharacter()
    {
        transform.Translate(MoveTarget * Speed * Time.deltaTime);
        MoveCamera();
    }

    private void MoveCamera()
    {
        Camera.main.transform.position = transform.position + _cameraOffset;
    }
}
