﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Bag : MonoBehaviour
{
    public static Bag instance;

    [SerializeField] private Text _moneyText;

    public Action<int> onMoneyChange;

    public int Money
    {
        get
        {
            return _money;
        }
        set
        {
            UpdateMoneyCount(value);
        }
    }

    private int _money;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this.gameObject);
        Money = 0;
    }

    public void AddMoney(int amount)
    {
        UpdateMoneyCount(amount);
    }

    public void RemoveMoney(int amount)
    {
        UpdateMoneyCount(-amount);
    }

    private void UpdateMoneyCount(int amount)
    {
        _money += amount;
        UpdateMoenyInfo();
    }

    private void UpdateMoenyInfo()
    {
        onMoneyChange?.Invoke(_money);
        _moneyText.text = $"Money: {_money}";
    }
}
