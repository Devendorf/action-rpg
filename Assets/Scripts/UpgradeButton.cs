﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class UpgradeButton : MonoBehaviour
{
    [SerializeField] private Image _costFill;
    [SerializeField] private int _cost;
    private Button _buyBtn;

    private void Start()
    {
        _buyBtn = GetComponent<Button>();
        Bag.instance.onMoneyChange += FillCost;
        _buyBtn.onClick.AddListener(BuyUpgrade);
    }

    private void OnDestroy()
    {
        Bag.instance.onMoneyChange -= FillCost;
    }

    private void BuyUpgrade()
    {
        Bag.instance.RemoveMoney(_cost);
    }

    private void FillCost(int amount)
    {
        float currentFill = 1 - (float)amount / _cost;
        _costFill.fillAmount = currentFill;
        if (currentFill == 0)
            _buyBtn.enabled = true;
    }
}
