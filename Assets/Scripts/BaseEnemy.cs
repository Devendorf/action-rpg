﻿using System.Collections;
using UnityEngine;

public class BaseEnemy : BaseCharacter
{
    [SerializeField] private float _agroRadius;
    [SerializeField] private LayerMask _playerMask;
    [SerializeField] private int _reward;

    private bool _targeted;

    private void FindTarget()
    {
        Collider2D[] withinAggroColliders = Physics2D.OverlapCircleAll(transform.position, _agroRadius, _playerMask);
        if (withinAggroColliders.Length > 0)
        {
            _movement.Move(withinAggroColliders[0].gameObject.transform.position);
            _targeted = true;
        }
        else
        {
            _movement.StopMovement();
            _targeted = false;
        }
    }

    protected override void Initial()
    {
        base.Initial();
        _weapon.EnemyType = typeof(PlayerCharacter);
        _weapon.Damage = 5;
    }

    protected override void Die()
    {
        base.Die();
        Bag.instance.AddMoney(_reward);
        StartCoroutine(DestroyBody());
    }

    private IEnumerator DestroyBody()
    {
        yield return new WaitForSeconds(5);
        Destroy(this.gameObject);
    }

    private void FixedUpdate()
    {
        if (isAlive)
        {
            FindTarget();
            if (_movement.DistanceRemain <= 2 && _targeted)
                base.Attack();
        }
    }
}
