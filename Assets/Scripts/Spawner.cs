﻿using System.Collections;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private GameObject _mobType;
    [SerializeField] private float _delay;

    private void Start()
    {
        StartCoroutine(Spawn());   
    }

    private IEnumerator Spawn()
    {
        WaitForSeconds delay = new WaitForSeconds(_delay);
        while (true)
        {
            Instantiate(_mobType, this.transform);
            yield return delay;
        }
    }
}
