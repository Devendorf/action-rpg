﻿using UnityEngine;

[RequireComponent(typeof(Movement))]
[RequireComponent(typeof(CharacterAnimator))]
public class BaseCharacter : MonoBehaviour
{
    [SerializeField] protected int _health;
    [SerializeField] private int _damage;
    [SerializeField] protected Weapon _weapon;

    protected Movement _movement;
    protected CharacterAnimator _characterAnimator;
    public bool isAlive;

    private void Start()
    {
        Initial();
        _characterAnimator.onAttackStateChange += SetWeaponEnabled;
        isAlive = true;
    }

    private void OnDestroy()
    {
        _characterAnimator.onAttackStateChange -= SetWeaponEnabled;
    }

    private void SetWeaponEnabled(bool value)
    {
        _weapon.SetActive(value);
    }

    protected virtual void Initial()
    {
        _movement = GetComponent<Movement>();
        _characterAnimator = GetComponent<CharacterAnimator>();
    }

    public virtual void TakeDamage(int amount)
    {
        if (isAlive)
        {
            _health -= amount;
            if (_health <= 0)
                Die();
            else
                _characterAnimator.TakeDamage();
        }
    }

    protected void Attack()
    {
        _characterAnimator.Attack();
    }

    protected virtual void Die()
    {
        _movement.speed =0;
        _characterAnimator.Die();
        isAlive = false;
        _weapon.SetActive(false);
    }
}
