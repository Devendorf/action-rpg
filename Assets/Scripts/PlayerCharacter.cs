﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(SpriteRenderer))]
public class PlayerCharacter : BaseCharacter
{
    [SerializeField] private Image _hpFill;
    private SpriteRenderer _spriteRenderer;
    private int _maxHealth;

    public override void TakeDamage(int amount)
    {
        base.TakeDamage(amount);
        FillHealthBar();
    }

    private void FillHealthBar()
    {
        float currentFill = (float)_health / _maxHealth;
        _hpFill.fillAmount = currentFill;
    }

    protected override void Initial()
    {
        base.Initial();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _weapon.EnemyType = typeof(BaseEnemy);
        _weapon.Damage = 10;
        _maxHealth = _health;
    }

    public void Upgrade()
    {
        _characterAnimator.animator.runtimeAnimatorController = Resources.Load("HeroKnight_AnimController 1") as RuntimeAnimatorController;
        _spriteRenderer.sprite = Resources.Load("HeroKnightOrange2_0") as Sprite;
        _weapon.Damage += 10;
        _maxHealth += 50;
        FillHealthBar();
    }

}
