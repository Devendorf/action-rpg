﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class PlayerAnimator : CharacterAnimator
{
    private SpriteRenderer _spriteRenderer;

    protected override void Initial()
    {
        base.Initial();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        AttackButton.onAttack += Attack;
    }

    private void OnDestroy()
    {
        AttackButton.onAttack -= Attack;
    }

    public override void Move(Vector3 target)
    {
        if (target != Vector3.zero)
        {
            base.Move(target);
        }
        else
        {
            base.Stop();
        }
    }

    public override void Die()
    {
        base.Die();
        AttackButton.onAttack -= Attack;
    }

    protected override void SetFacingDirection(Vector3 target)
    {
        if (target.x > 0)
        {
            _spriteRenderer.flipX = false;
            _facingDirection = 1;
        }
        else
        {
            _spriteRenderer.flipX = true;
            _facingDirection = -1;
        }
    }

    protected override void SetCurrentAttackState()
    {
        _currentAttackState = _timeSinceAttack > _comboTime
            ? 1
            : (_currentAttackState % 3) + 1;
    }
}
