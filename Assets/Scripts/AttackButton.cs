﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class AttackButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public delegate void AttackAction();
    public static AttackAction onAttack;
    public static AttackAction onStoped;

    private Coroutine _attackCoroutine;

    private IEnumerator KeepAttack()
    {
        while (true)
        {
            onAttack();
            yield return null;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _attackCoroutine = StartCoroutine(KeepAttack());
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        StopCoroutine(_attackCoroutine);
        onStoped();
    }
}
