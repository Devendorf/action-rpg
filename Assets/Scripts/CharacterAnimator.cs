﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(Animator))]
public class CharacterAnimator : MonoBehaviour
{
    public Animator animator;
    protected int _facingDirection = 1;
    protected float _timeSinceAttack = 0.0f;
    private float _delayToIdle = 0.0f;

    public Action<bool> onAttackStateChange;
    [SerializeField] float _attacksPerSecond;
    [SerializeField] protected float _comboTime;
    [SerializeField] GameObject _attackArea;
    protected int _currentAttackState = 0;
    private bool _attackReady = true;
    private bool isAttacking;

    public bool IsAttacking
    {
        get
        {
            return isAttacking;
        }
        set
        {
            if (isAttacking != value)
                onAttackStateChange(value);
            isAttacking = value;
        }
    }

    private float _attackDelay
    {
        get
        {
            return 1f / _attacksPerSecond;
        }
    }

    void Awake()
    {
        Initial();
    }

    protected virtual void Initial()
    {
        animator = GetComponent<Animator>();
    }

    public void Attack()
    {
        if (_attackReady)
        {
            SetCurrentAttackState();
            animator.SetTrigger("Attack" + _currentAttackState);
            _attackReady = false;
            IsAttacking = !_attackReady;
            StartCoroutine(Cooldown());
            _timeSinceAttack = 0.0f;
        }
    }

    public void TakeDamage()
    {
        animator.SetTrigger("Hurt");
    }
    public virtual void Die()
    {
        animator.SetBool("noBlood", false);
        animator.SetTrigger("Death");
    }

    protected virtual void SetCurrentAttackState()
    {
        _currentAttackState = _timeSinceAttack > _comboTime
            ? 1
            : (_currentAttackState % 2) + 1;
    }

    private IEnumerator Cooldown()
    {
        yield return new WaitForSeconds(_attackDelay);
        _attackReady = true;
        IsAttacking = !_attackReady;
    }

    public void Stop()
    {
        animator.SetInteger("AnimState", 0);
    }

    protected virtual void SetFacingDirection(Vector3 target)
    {
        Vector3 direction = target - transform.position;
        if (direction.x > 0)
        {
            GetComponent<SpriteRenderer>().flipX = false;
            _facingDirection = 1;
        }
        else
        {
            GetComponent<SpriteRenderer>().flipX = true;
            _facingDirection = -1;
        }
    }

    public virtual void Move(Vector3 target)
    {
        SetFacingDirection(target);
        RotateCharacter();
        _delayToIdle = 0.05f;
        animator.SetInteger("AnimState", 1);
        animator.SetBool("Grounded", true);

    }

    protected void RotateCharacter()
    {
        float yRotation = _facingDirection == 1
            ? 0
            : 180;
        _attackArea.transform.rotation = new Quaternion(0, yRotation, 0, _attackArea.transform.rotation.w);
    }

    void Update()
    {
        _timeSinceAttack += Time.deltaTime;

        float inputX = Input.GetAxis("Horizontal");

        _delayToIdle -= Time.deltaTime;
        if (_delayToIdle < 0)
            animator.SetInteger("AnimState", 0);

    }
}
