﻿using UnityEngine;

public class Weapon : MonoBehaviour
{
    public System.Type EnemyType;
    private BoxCollider2D _collider;
    private bool _enabled;
    public int Damage;

    private void Start()
    {
        _collider = GetComponent<BoxCollider2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        BaseCharacter enemy;
        if (collision.gameObject.TryGetComponent<BaseCharacter>(out enemy))
            if (enemy.GetType() == EnemyType)
                enemy.TakeDamage(Damage);
    }

    public void SetActive(bool value)
    {
        _enabled = value;
        _collider.enabled =_enabled;
    }
}
